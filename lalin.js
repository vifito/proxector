
function wrapText(context, text, x, y, line_width, line_height) {
  var line = '';
  var paragraphs = text.split('\n');
  for (var i = 0; i < paragraphs.length; i++) {
    var words = paragraphs[i].split(' ');
    for (var n = 0; n < words.length; n++) {
      var testLine = line + words[n] + ' ';
      var metrics = context.measureText(testLine);
      var testWidth = metrics.width;
      if (testWidth > line_width && n > 0) {
        context.strokeText(line, x, y);
        context.fillText(line, x, y);
        line = words[n] + ' ';
        y += line_height;
      }
      else {
        line = testLine;
      }
    }
    context.strokeText(line, x, y);
    context.fillText(line, x, y);
    y += line_height;
    line = '';
  }
}

function render(texto) {
  console.log('render');

  var bg = new Image();
  bg.onload = function () {
    ctx.drawImage(bg, 0, 0);

    var yPos = 90;
    if (texto.length < 50) {
      yPos = 180;
    }
    if (texto.length < 60) {
      yPos = 140;
    }
    
    wrapText(ctx, texto, 190, yPos, 300, 45);
  };
  bg.src = './concello.jpg';

  ctx.font = '42px Oswald';
  //ctx.fillStyle = 'gradient';
  ctx.fillStyle = 'rgba(255, 255, 226, 0.7)';
  ctx.lineWidth = 1;
  ctx.strokeStyle = 'rgba(0, 0, 0, 1)';
  ctx.textAlign = 'center';
  //60,100 - 345,340
  //wrapText(ctx, "Welcome to the Jungle", 60, 100, 340, 20)
}

function descargar() {
  var link = document.querySelector('#lnk');
  link.setAttribute('download', 'lalin.png');
  link.setAttribute('href', canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"));
  link.click();
  // var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
  // window.location.href = image;
}

function init(canvas) {
  console.log('init');
  var fld = document.querySelector('#msg');

  fld.addEventListener("blur", function () {
    render(fld.value);
    console.log(fld);
  }, false);

  document.querySelector('#dwnld').addEventListener("click", function() {
    descargar();
  }, false);

  //render(fld.value);
  document.fonts.load('10pt "Oswald"').then(function() {render(fld.value)});
}
